Panel Template
=================================

Create a single page site divided into panels. Features a fixed navigation that updates on scroll and callbacks to perform functions on the panel in focus. Supports touch devices with a magnetic lock-to-panel functionality using swipe and drag.

In the instructions below, it is assuming use of a global variable SP, on which the two Manager objects are defined as properties.

Options
-------

### navigation ###

_(selector)_ 

Selector for element containing navigation anchor links.

### scroller ###

_(selector)_ 

The element containing all panels.

### panel: '.panel' ###

_(selector)_

Selector for panel elements (default is class 'panel').

### nextBtn: '.next' ###

_(selector)_

Selector of Next Button control.

### prevBtn: '.prev' ###

_(selector)_

Selector of Previous Button control.

### afterSlide ###

_(function($panel))_

The default function adds a class "in-view" to the panel in focus. This can be replaced by any custom function, and receives the panel in focus as its argument. On desktop currently, afterSlide is called on scroll unless the navigation has been used to smoothScroll to a particular panel, so be careful of putting too much complex functionality into it for now.

Usage
-----

Instantiate the ScrollManager as follows:

	var scroller = new ScrollManager({
		navigation: 'nav',
		scroller: '#scroller'
	});
	
	
Markup Example
-----

Navigation: Anchor links should point to actual panel IDs so that they remain functional even without JavaScript. It is recommended to have the navigation outside the Scroller. The Navigation element will receive a "stuck" class when it reaches the top of the window.

	<nav>
		<ul>
			<li><a href="#first-panel">First Panel</a></li>
			<li><a href="#second-panel">Second Panel</a></li>
			<li><a href="#third-panel">Third Panel</a></li>
		</ul>
	</nav>

Scroller: The recommended setup is to have a container with first level children being the panels.

	<div id="scroller">
		<section class="panel">
			First Panel Content
		</section>
		<section class="panel">
			Second Panel Content
		</section>
		<section class="panel">
			Third Panel Content
		</section>
	</div>

	
Dependencies
-------------------

jQuery (currently used/tested with 1.10.2)
Modernizr (touch feature detection)
HammerJS (http://eightmedia.github.io/hammer.js/)
jQuery smoothScroll (https://github.com/kswedberg/jquery-smooth-scroll)

Highly Recommended
-------------------

FastClickJS (https://github.com/ftlabs/fastclick)


