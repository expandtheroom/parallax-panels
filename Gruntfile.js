module.exports = function(grunt) {

  /*-------------------------------------------- */
  /** Project Config */
  /*-------------------------------------------- */
  
  var lessOptionsPaths = ['less'],
    lessFiles = {
      'css/style.css': 'less/style.less'
    };

  var jsOptionsPaths = ['js'],
    jsConcatFiles = ['js/plugins/*.js'],
    jsHintFiles = ['js/scripts.js', 'js/panel-manager.js'],
    uglifyFiles = { 'js/scripts.min.js': ['js/panel-manager.js', 'js/scripts.js'] };

  grunt.initConfig({
    
    watch: {
      options: {
        interval: 20
      },
      less: {
        files: ['less/*.less'],
        tasks: ['less:development']
      },
      all: {
        files: ['*/**', '!node_modules/*/**', '!less/*.less'],
        options: {
          livereload: true
        }
      },
    },

    less: {

      development: {
        options: {
          paths: lessOptionsPaths
        },
        files: lessFiles
      }

    },

    cssmin: {
      minify: {
        files: {
          'css/style.min.css': 'css/style.css'
        }
      }
    },

    uglify: {

      development: {
        files: uglifyFiles
      }

    },

    concat: {
      options: {
        separator: ';'
      },
      development: {
        src: jsConcatFiles,
        dest: 'js/vendor.min.js'
      },
      prod: {
        src: ['js/vendor.min.js', 'js/scripts.min.js'],
        dest: 'js/scripts.final.min.js'
      }
    },

    jshint: {
      files: jsHintFiles
    }

  });

  /*-------------------------------------------- */
  /** Load Tasks */
  /*-------------------------------------------- */
  
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-concat');

  /*-------------------------------------------- */
  /** Register Tasks */
  /*-------------------------------------------- */

  grunt.registerTask('dist', ['less', 'cssmin', 'uglify', 'concat' ]);
};