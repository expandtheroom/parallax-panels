var SP = window.SP || {};

;(function($){

	// Manager Objects

	var ScrollManager = function(opts) {
		// Cached Dom Elements
		this.$win = $(window);
		this.$doc = $(document);

		// Navigation object
		this.navigation = new NavManager(opts.navigation, opts.scroller);

		// Elements and selectors
		this.$scroller = $(opts.scroller);
		this.$panels = opts.panel ? $(opts.panel) : $('.panel');
		this.$prevBtn = opts.prevBtn ? $(opts.prevBtn) : $('.prev');
		this.$nextBtn = opts.nextBtn ? $(opts.nextBtn) : $('.next');
		this.panelSelector = opts.panel || '.panel';

		// Callbacks
		this.afterSlide = opts.afterSlide || this.afterSlide;

		// Calculated Variables and Tracking
		this.$currentPanel = window.location.hash && $(window.location.hash).attr('id') ? $(window.location.hash) : $(this.panelSelector + ':first-child');
		this.latestKnownScrollY = 0;
		this.cushion = 0;

		console.log(this.$panels);

		this.init();
	};

	$.extend(ScrollManager.prototype, {

		setFullScreen: function() {
			console.log(this.$panels);
			this.$panels.css('height', this.$win.height());
			this.cushion = this.$win.height() * 0.5;

			if (Modernizr.touch) {
				this.$scroller.css('height', this.$win.height());
			}
		},

		afterSlide: function($container) {
			var $panel = $container ? $container : this.$currentPanel;
			$panel.addClass('in-view').siblings(this.panelSelector).removeClass('in-view');
		},

		getPanelInFocus: function(position) {
			var self = this;

			var $currentPanel;

			this.$panels.each(function(i, el) {
				var offset = $(el).offset().top;

				if (offset >= position - self.cushion && offset <= position + self.cushion) {
					$currentPanel = $(el);
				}
			});

			return $currentPanel;
		},

		lockToPanel: function($panel) {
			var self = this;

			var $target = $panel ? $panel : this.$currentPanel,
				targetId = '#' + $target.attr('id');

			$.smoothScroll({
			    scrollTarget: targetId,
			    scrollElement: self.$scroller,
			    afterScroll: function() {
			    	self.$doc.trigger('afterScroll', targetId);
			    	self.$currentPanel = $target;
			    }
			});
		},

		nextPanel: function(e) {
			if (e) e.preventDefault();

			var self = this;
			var scrollElement = Modernizr.touch ? self.$scroller : null;

			var $nextPanel = this.$currentPanel.next(this.panelSelector),
				nextPanelID = '#' + $nextPanel.attr('id');

			$.smoothScroll({
			    scrollTarget: nextPanelID,
			    scrollElement: scrollElement,
			    afterScroll: function() {
			    	self.$doc.trigger('afterScroll', nextPanelID);
			    	self.$currentPanel = $nextPanel;
			    }
			});

		},

		prevPanel: function(e) {
			if (e) e.preventDefault();

			var self = this;
			var scrollElement = Modernizr.touch ? self.$scroller : null;

			var $nextPanel = this.$currentPanel.prev(this.panelSelector),
				nextPanelID = '#' + $nextPanel.attr('id');

			$.smoothScroll({
			    scrollTarget: nextPanelID,
			    scrollElement: scrollElement,
			    afterScroll: function() {
			    	self.$doc.trigger('afterScroll', nextPanelID);
			    	self.$currentPanel = $nextPanel;
			    }
			});
		},

		scrollHandler: function() {			
			var self = this;
			this.latestKnownScrollY = this.$win.scrollTop();

			// handle nav update
			if (this.latestKnownScrollY >= self.navigation.offset) {
				self.navigation.stick();
			} else {
				self.navigation.unstick();
			}

			// debounce if smooth scroll is on
			if (!self.navigation.isScrolling) {
				self.$currentPanel = self.getPanelInFocus(self.latestKnownScrollY) || self.$currentPanel;

				var id = '#' + self.$currentPanel.attr('id');

				self.afterSlide(self.$currentPanel);
				self.navigation.setActiveLink(id);
			} else {
				self.$doc.on('afterScroll', function(e, href){
					self.$currentPanel = $(href);
					self.afterSlide(self.$currentPanel);
					self.navigation.setActiveLink(href);
				});
			}
		},

		touchHandler: function(e) {
			if (e.gesture.deltaTime < 299) return;
			
			var self = this;
			this.latestKnownScrollY = this.$win.scrollTop();

			// handle panel updates
			var panel = this.getPanelInFocus(self.latestKnownScrollY);

			this.$currentPanel = panel;
			this.lockToPanel(this.$currentPanel);
		},

		swipeHandler: function(e) {
			if (e.gesture.deltaTime > 300) return;

			var direction = e.type;
			var nextPanel = direction == 'swipeup' ? this.$currentPanel.next(this.panelSelector) : this.$currentPanel.prev(this.panelSelector);

			if (!nextPanel.attr('id')) return;

			this.lockToPanel(nextPanel);
		},

		init: function() {
			var self = this;

			// set full screen
			this.setFullScreen();

			// animate current panel
			this.afterSlide(this.$currentPanel);

			// set up listeners
			this.$doc.on('afterScroll', function(e, href){
				self.$currentPanel = $(href);
				self.afterSlide($(href));
				self.navigation.setActiveLink(href);
			});

			this.$prevBtn.on('click', $.proxy(this.prevPanel, this));
			this.$nextBtn.on('click', $.proxy(this.nextPanel, this));

			// Conditional based on touch support
			if (Modernizr.touch) {
				// refresh gives a weird experience on mobile, so scroll to top on init
				self.lockToPanel(self.$currentPanel);

				// listen for gesture events
				this.$scroller.on('swipeup swipedown', $.proxy(this.swipeHandler, this));
				this.$scroller.on('dragend', $.proxy(this.touchHandler, this));

				// prevent elastic overscroll when you hit top and bottom
				$(this.panelSelector + ':first-child').on('swipedown dragdown', function(e){ e.gesture.preventDefault(); return; })
				$(this.panelSelector + ':last-child').on('swipeup dragup', function(e){ e.gesture.preventDefault(); return; })
				self.navigation.$el.on('swipedown dragdown', function(e){ e.gesture.preventDefault(); })

				// reset height on orientation change
				this.$win.on('orientationchange', function() {
					self.setFullScreen.call(self);
					setTimeout(function() {
						self.lockToPanel(self.$currentPanel);
					}, 0);
				});
				
				//hook up fastclick
				FastClick.attach(document.body);
			} else {
				// desktop listens to scroll
				this.$win.on('scroll', $.proxy(this.scrollHandler, this));
			}
		}

	});

	var NavManager = function(el, scrollElement) {
		this.$el = $(el);
		this.offset = this.$el.offset().top;
		this.$parent = this.$el.parent();
		this.$scrollElement = $(scrollElement);
		this.$doc = $(document);
		this.$win = $(window);
		this.isScrolling = false;

		this.init();
	};

	$.extend(NavManager.prototype, {

		setActiveLink: function(href) {
			var $activeLink = $('a[href=' + href + ']');

			this.$el.find('a').removeClass('active');
			$activeLink.addClass('active');
		},

		stick: function() {
			this.$el.addClass('stuck');
		},

		unstick: function() {
			this.$el.removeClass('stuck');
		},

		init: function() {
			var self = this;

			// set scrollElement if touch
			if (!Modernizr.touch) {
				this.$scrollElement = null;
			}

			// init smoothscrolling
			this.$el.on('click', 'a', function(e){
				e.preventDefault();
				var href = $(this).attr('href'),
					id = href.replace('#','');

				$.smoothScroll({
					offset: 0,
					scrollTarget: href,
					scrollElement: self.$scrollElement,
					beforeScroll: function() {
						self.isScrolling = true;
					},
					afterScroll: function() {
						self.$doc.trigger('afterScroll', href);
						self.isScrolling = false;
						self.setActiveLink(href);
					}
				});
			});
		}
	});

	// Export
	SP.ScrollManager = ScrollManager;
        
})(jQuery);