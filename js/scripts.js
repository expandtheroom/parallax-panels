var SP = window.SP || {};

(function($){

	var scroller = new SP.ScrollManager({
		navigation: 'nav',
		scroller: '#scroller'
	});

	// hammer js
	$(document.body).hammer({
		swipe_velocity: 0.4
	});

})(jQuery);